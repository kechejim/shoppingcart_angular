
myservice = angular.module('myservice', []);

myservice.value('Host','http://192.168.6.129/spdelivery/api/api.php')
	.factory('AppSetting',['Host', '$http', function(Host, $http) {
	   return {

			//set locale
			SetLocal: function($scope, code) {
				return  $http({
							url: Host + "/setlocal/" + code,
							method: "GET",
							headers: {'Content-Type': 'application/x-www-form-urlencoded'}
						})
						.success(function (data, status, headers, config) {

							console.log('Get Products  ' + status );
							
						})
						.error(function (data, status, headers, config) {
									
						});		
			}
	   }
	}])
	.factory('ProductApi',['Host', '$http', function(Host, $http) {
	   return {
			GetProductsByCategory: function($scope, d) {

				return  $http({
							url: Host + "/getProductsByCategory/" + d.category + "/" + d.page,
							method: "GET",
							headers: {'Content-Type': 'application/x-www-form-urlencoded'}
						})
						.success(function (data, status, headers, config) {

							console.log('Get Products  ' + status + ', reddit.product :' + $scope.Reddit.products);

							console.log('response :' + data.length);

							for (var i = 0; i < data.length; i++) {
								$scope.Reddit.products.push(data[i]);
  						    }

							if (data.length == 0)
							{
								$scope.Reddit.page--;					
							}

							$scope.Reddit.busy = false;					

						})
						.error(function (data, status, headers, config) {
									
						});		
			},
			GetCategories: function($scope) {
			
				return  $http({
							url: Host + "/getcategories",
							method: "GET",							
							headers: {'Content-Type': 'application/x-www-form-urlencoded'}
						})
						.success(function (data, status, headers, config) {
			
							$scope.category = data;
							console.log("response status : " + status);

							return data;							
						})
						.error(function (data, status, headers, config) {
							console.log('error : ' + status);		
						});						
			}


	   }
  }]);
