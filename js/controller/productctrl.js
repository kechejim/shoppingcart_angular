
var productController = function($scope, $rootScope, $routeParams, $location, ProductApi) {
  
	// Reddit constructor function to encapsulate HTTP and pagination logic
	var Reddit = function() {
		this.products = [];
		this.busy = false;
		this.page = 1;
	};

	Reddit.prototype.nextPage = function() {
		if (this.busy) return;
		
		this.busy = true;
		this.page++; 

		var d = {	
			'category': $routeParams.category,
			'page': this.page
		};

		ProductApi.GetProductsByCategory($scope, d);
	};
	/*-----------------------------------------------------------------------------*/
		
	$scope.Reddit = new Reddit();

	$scope.selcategory = $routeParams.category;
	
	var d = {	
			'category': $routeParams.category,
			'page': 1
		};

	var products = ProductApi.GetProductsByCategory($scope, d);

};

productController.$inject = ["$scope", "$rootScope", "$routeParams", "$location", "ProductApi"];