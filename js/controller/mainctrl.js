
var mainController = function($scope, $rootScope, $location, ProductApi) {
//angularApp.controller('mainController', function($scope, $rootScope, $location, ProductApi) {

	$scope.NavDesktop 	= './partials/nav-desktop.html';
	$scope.NavSidebar	= './partials/nav-sidebar.html';
	$scope.Carousel		= './partials/carousel.html';
	
	ProductApi.GetCategories($scope);
	
	$rootScope.addToCart = function (item) {
		var cartitem={};
		cartitem['id']=$rootScope.cart.length+1;
		cartitem['title']=item.title_en;
		$rootScope.cart.push(cartitem);
	}

	$rootScope.checkOut = function () {
		if ($rootScope.cart.length == 0) {
			$rootScope.errorText = "No Item to Checkout";
		}
		console.log($scope.mobile);
		if (typeof ($scope.mobile) == 'undefined') {
			console.log("membership");
			$location.path("membership");
		} else {
			console.log("checkout");
			$location.path("checkout");
		}
	}	

};

mainController.$inject = ["$scope", "$rootScope", "$location", "ProductApi"];