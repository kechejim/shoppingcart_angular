var angularApp = angular.module('angularApp', ['myservice', 'ngRoute', 'infinite-scroll']);

angularApp.run(function($rootScope, $http, $location) {
	//$http.get('api/api.php/products').success(function(ret){
	//	$rootScope.products = ret;
	//});

	$rootScope.cart=[];

});

angularApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
    when('/home', {
      templateUrl: 'partials/home.html',
      controller: 'mainController'
    }).
    when('/products/:category', {
      templateUrl: 'partials/products.html',
      controller: 'productController'
    }).
    when('/cart', {
      templateUrl: 'partials/cart.html',
      controller: 'mainController'
    }).
    when('/membership', {
      templateUrl: 'partials/membership.html',
      controller: 'mainController'
    }).
    when('/checkout', {
      templateUrl: 'partials/checkout.html',
      controller: 'mainController'
    }).
    otherwise({
      redirectTo: '/home'
    });
  }
]);

//
//	JQuery Only after here
//


