<!DOCTYPE html>
<html lang="en" ng-app="angularApp">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>document.cookie='resolution='+Math.max(screen.width,screen.height)+'; path=/';</script>
  <title>SP Trade</title>

  <!-- Bootstrap -->
  <link href="css/default/bootstrap.min.css" rel="stylesheet">
  <link href="css/default/font-awesome.min.css" rel="stylesheet">
  <link href="css/default/royalslider.css" rel="stylesheet">
  <link href="css/default/rs-default.css" rel="stylesheet">
  <link href="css/app.css" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

  <!--     lib files    -->  
  <script src="js/lib/jquery/jquery-1.11.1.min.js"></script>
  <script src="js/lib/bootstrap/bootstrap.min.js"></script>
  <script src="js/lib/angular/angular.min.js"></script>
  <script src="js/lib/angular/angular-route.min.js"></script>
  <script src="js/lib/jquery/jquery.easing-1.3.js"></script>
  <script src="js/lib/jquery/jquery.royalslider.min.js"></script>
</head>

<body ng-controller='mainController' >

  <div class="container">

    <div class="row" ng-include='NavDesktop'>      

    </div>

    <div class="row">
      
	  <div class="col-md-2 block" ng-include="NavSidebar">      

      </div>

      <div class="col-md-10 block" ng-include='Carousel'>                
      </div>
    </div>
    
  </div>

  <!--    angular files    -->  
  <script src="js/app.js"></script>

  <script src="js/controller/mainctrl.js"></script>
  <script src="js/controller/productctrl.js"></script>

  <script src="js/service/api.js"></script>
  <script src="js/directive/infinite-scroll.js"></script>
</body>

</html>