<?php
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Slim Framework
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

// Redbean PHP
require_once('rb.php');
require_once('rbsetup.php');

//  PHP Globals
ini_set('memory_limit', '-1');
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
date_default_timezone_set('Asia/Hong_Kong');
session_start();

//  Slim routes
$app = new \Slim\Slim();

$authenticate = function ($app) {
    return function () use ($app) {
        if (!isset($_SESSION['uid'])) {
            $app->redirect('../../#/login');
        }
    };
};

// variables
$app->local = 'en';

// get requests
$app->get('/setlocal/:code', function($code) use ($app){

	$app->local = $code;

});

//getting all categroy info
$app->get('/getcategories', function() use ($app){
	
	$table = 'category_'. $app->local;

	$categoryList = R::getAll ("select * from ". $table);

	if (sizeof($categoryList) == 0) {
		$ret['status'] = 'no category';
		echo json_encode($ret);
		exit;
	}

	echo  json_encode($categoryList);
});

// getting products by category and page number
$app->get('/getProductsByCategory/:categoryCode/:page', function($categoryCode, $page) use ($app){

	$pagesize = 3;
	$start = ($pagesize * ($page -1));

	$table = 'product_'. $app->local;

	//infinite scroll
	$limit = ' limit ' . $start . ',' . $pagesize;
	
	//where
	$where = "";
	$where = addWhereOr($where, "`categorycode` like '%".$categoryCode."%'");

	//making sql
	$statsql = 'select * from '. $table . '  '. $where . $limit;

	//run query
	$stat = R::getAll ($statsql);

	if (sizeof($stat) == 0) {
		$data['items']=0;
		echo json_encode($data);
		exit;
	}

	echo json_encode($stat);
   	
});

//
//	Generic table with paging
//
function addWhere($where, $clause) {
	if ($where == "") {$where = " where ";} else {$where = $where . " and ";};
	return $where . $clause;
}
function addWhereOr($where, $clause) {
	if ($where == "") {$where = " where ";} else {$where = $where . " or ";};
	return $where . $clause;
}

$app->get('/getinvoiceno', function () {
	$invno = R::getAll ("select max(invoiceno) from invoice");


	$dateString = date ('Ymd');
	if (sizeof($invno) == 0) {
		$data['invoiceno']='I' . $dateString . '001';
		echo json_encode($data);
		exit;
	}

	echo  json_encode($invno);
});

$app->get('/:table', function ($table) use ($app) {

	//
	//	Handle paging
	//
	$pagesize=$_REQUEST['pagesize'];
	$start=0;
	$currentPage=1;
	$where = "";
	$request = $app->request();
	$body = $request->getBody();
	$input = json_decode($body); 
	if($request->get('page')) {
		$currentPage = $request->get('page');
		$start = ($pagesize * ($currentPage -1));
	}

	//
	//	Fields selection
	//
	$fields = "*";


	//
	// Filter conditions here
	//
	if (isset($_REQUEST['item'])) { 
		$where = addWhereOr($where, "`code` like '%".$_REQUEST['item']."%'");
		$where = addWhereOr($where, "`desc` like '%".$_REQUEST['item']."%'");
	};

	if (isset($_REQUEST['client'])) { 
		$where = addWhereOr($where, "`name` like '%".$_REQUEST['client']."%'");
		$where = addWhereOr($where, "`contact` like '%".$_REQUEST['client']."%'");
	};

	//
	//	Page Size
	//
	$limit = ' limit ' . $start . ',' . $pagesize;
	if (isset($_REQUEST['export']))
	{
		$limit = " limit 64000";
	}


	//
	//	Summary columns here
	//
	$summcols = "";

	$statsql = 'select count(*) as count ' . $summcols . ' from ' . $table . ' ' . $where;
	$sql = 	'select ' . $fields . ' from ' . $table . ' '
	. $where
	. $limit;


	//echo $sql;

	$stat = R::getAll ($statsql);

	if (sizeof($stat) == 0) {
		$data['items']=0;
		echo json_encode($data);
		exit;
	}
	$all = R::getAll ($sql);


	//
	//	Paging columns here
	//	
	$data['itemcount']=sizeof($all);
	$data['currentpage']=$currentPage*1;
	$data['totalitems']=$stat[0]['count'];
	$data['totalpages']=ceil($stat[0]['count'] / $pagesize);

	//
	//	Stuff data here
	//
	$data['items']=$all;

	//
	//	Export code here
	//
	if (isset($_REQUEST['export']))
	{
		download_send_headers($table . "_export_" . date("Y-m-d") . ".csv");
		echo array2csv($data['items']);
	} else {
		$app->response()->header('Content-Type', 'application/json');
		if($table=="users"){
			if($_SESSION['level'] == 2){
				echo json_encode($data);
			}else{
				echo "errorprivileges";
			}
		}else{
			echo json_encode($data);
		}
	}
});

$app->run();

exit;


?> 