-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 14, 2014 at 09:00 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `spdelivery`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE IF NOT EXISTS `cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerid` int(11) NOT NULL,
  `sku` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `unit` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unitprice` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`id`,`customerid`),
  KEY `fk_customerid_idx` (`customerid`),
  KEY `fk_sku_idx` (`sku`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `category_ch`
--

CREATE TABLE IF NOT EXISTS `category_ch` (
  `code` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tag` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `category_en`
--

CREATE TABLE IF NOT EXISTS `category_en` (
  `code` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `tag` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `desc` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`code`),
  UNIQUE KEY `id_UNIQUE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category_en`
--

INSERT INTO `category_en` (`code`, `tag`, `desc`) VALUES
('cond', 'Condiments', 'Condiments'),
('crab', 'Crabs', 'Import Crabs'),
('frut', 'Fruits', 'Fresh Fruits'),
('oyst', 'Oysters', 'Fresh Oysters'),
('tofu', 'Tofu', 'Organic Tofu'),
('vegt', 'Vegetables', 'Fresh Vegetables');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `deliveryaddress`
--

CREATE TABLE IF NOT EXISTS `deliveryaddress` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerid` int(11) DEFAULT NULL,
  `contact` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_customer_idx` (`customerid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerid` int(11) DEFAULT NULL,
  `orderdate` datetime DEFAULT NULL,
  `contact` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `orderdetail`
--

CREATE TABLE IF NOT EXISTS `orderdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` int(11) DEFAULT NULL,
  `sku` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `unit` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unitprice` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_order_idx` (`orderid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_ch`
--

CREATE TABLE IF NOT EXISTS `product_ch` (
  `sku` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `categorycode` varchar(8) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `unit` varchar(4) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `unitprice` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_en`
--

CREATE TABLE IF NOT EXISTS `product_en` (
  `sku` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `categorycode` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unitprice` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`sku`),
  KEY `fk_category_idx` (`categorycode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_en`
--

INSERT INTO `product_en` (`sku`, `categorycode`, `desc`, `unit`, `unitprice`) VALUES
('cd-01-002', 'cond', 'Sweet Cream 14oz x 12', 'box', '327'),
('cd-01-003', 'cond', 'Sweet Cream 14oz', 'bott', '40'),
('cd-01-004', 'cond', 'Condiment 1', 'pc', '31'),
('cd-01-005', 'cond', 'Condiment 2', 'pc', '32'),
('cd-01-006', 'cond', 'Condiment 3', 'pc', '33'),
('cd-01-007', 'cond', 'Condiment 4', 'pc', '34'),
('cd-01-008', 'cond', 'Condiment 5', 'pc', '35'),
('cd-01-009', 'cond', 'Condiment 6', 'pc', '36'),
('cd-01-010', 'cond', 'Condiment 7', 'pc', '37'),
('cd-01-011', 'cond', 'Condiment 8', 'pc', '38'),
('cd-01-012', 'cond', 'Condiment 9', 'pc', '21'),
('cd-01-013', 'cond', 'Condiment 10', 'pc', '22'),
('cd-01-014', 'cond', 'Condiment 11', 'pc', '23'),
('cd-01-015', 'cond', 'Condiment 12', 'pc', '24'),
('cd-01-016', 'cond', 'Condiment 13', 'pc', '25'),
('cd-01-017', 'cond', 'Condiment 14', 'pc', '26'),
('cd-01-018', 'cond', 'Condiment 15', 'pc', '27'),
('cd-01-019', 'cond', 'Condiment 16', 'pc', '28'),
('cd-01-020', 'cond', 'Condiment 17', 'pc', '29'),
('cd-01-021', 'cond', 'Condiment 18', 'pc', '30'),
('cd-01-022', 'cond', 'Condiment 19', 'pc', '31'),
('cd-01-023', 'cond', 'Condiment 20', 'pc', '32'),
('cd-01-024', 'cond', 'Condiment 21', 'pc', '33'),
('cd-01-025', 'cond', 'Condiment 22', 'pc', '34'),
('cd-01-026', 'cond', 'Condiment 23', 'pc', '35'),
('cd-01-027', 'cond', 'Condiment 24', 'pc', '36'),
('cd-01-028', 'cond', 'Condiment 25', 'pc', '37'),
('cd-01-029', 'cond', 'Condiment 26', 'pc', '38'),
('cd-01-030', 'cond', 'Condiment 27', 'pc', '39'),
('cd-01-031', 'cond', 'Condiment 28', 'pc', '40'),
('cd-01-032', 'cond', 'Condiment 29', 'pc', '41'),
('cd-01-033', 'cond', 'Condiment 30', 'pc', '42'),
('cd-01-034', 'cond', 'Condiment 31', 'pc', '43'),
('cd-01-035', 'cond', 'Condiment 32', 'pc', '44'),
('cd-01-036', 'cond', 'Condiment 33', 'pc', '45'),
('cd-01-037', 'cond', 'Condiment 34', 'pc', '46'),
('kf-01-004', 'frut', 'Peach Small', '6pcs', '110'),
('kf-01-005', 'frut', 'Tangerines', '6pcs', '130'),
('kf-01-006', 'frut', 'Oranges', '6pcs', '120'),
('kf-01-007', 'frut', 'Fruit 1', '6pcs', '99'),
('kf-01-008', 'frut', 'Fruit 1', '6pcs', '100'),
('kf-01-009', 'frut', 'Fruit 1', '6pcs', '101'),
('kf-01-010', 'frut', 'Fruit 1', '6pcs', '102'),
('kf-01-011', 'frut', 'Fruit 1', '6pcs', '103'),
('kf-01-012', 'frut', 'Fruit 1', '6pcs', '104'),
('kf-01-013', 'frut', 'Fruit 1', '6pcs', '105'),
('kf-01-014', 'frut', 'Fruit 1', '6pcs', '106'),
('kf-01-015', 'frut', 'Fruit 1', '6pcs', '107'),
('kf-01-016', 'frut', 'Fruit 1', '6pcs', '108'),
('kf-01-017', 'frut', 'Fruit 1', '6pcs', '109'),
('kf-01-018', 'frut', 'Fruit 1', '6pcs', '110'),
('kf-01-019', 'frut', 'Fruit 1', '6pcs', '111'),
('kf-01-020', 'frut', 'Fruit 1', '6pcs', '112'),
('kf-01-021', 'frut', 'Fruit 1', '6pcs', '113'),
('kf-01-022', 'frut', 'Fruit 1', '6pcs', '114'),
('kf-01-023', 'frut', 'Fruit 1', '6pcs', '115'),
('kf-01-024', 'frut', 'Fruit 1', '6pcs', '116'),
('kf-01-025', 'frut', 'Fruit 1', '6pcs', '117'),
('kf-01-026', 'frut', 'Fruit 1', '6pcs', '118'),
('kf-01-027', 'frut', 'Fruit 1', '6pcs', '119'),
('kf-01-028', 'frut', 'Fruit 1', '6pcs', '120'),
('kf-01-029', 'frut', 'Fruit 1', '6pcs', '121'),
('kf-01-030', 'frut', 'Fruit 1', '6pcs', '122'),
('kf-01-031', 'frut', 'Fruit 1', '6pcs', '123'),
('kf-01-032', 'frut', 'Fruit 1', '6pcs', '124'),
('kf-01-033', 'frut', 'Fruit 1', '6pcs', '125'),
('vg-01-002', 'vegt', 'Organic Spinich', 'bag', '40'),
('vg-01-003', 'vegt', 'Mushrooms Large', 'bag', '55'),
('vg-01-004', 'vegt', 'Mushrooms Small', 'bag', '35'),
('vg-01-005', 'vegt', 'Veg 1', 'bag', '24'),
('vg-01-006', 'vegt', 'Veg 1', 'bag', '25'),
('vg-01-007', 'vegt', 'Veg 1', 'bag', '26'),
('vg-01-008', 'vegt', 'Veg 1', 'bag', '27'),
('vg-01-009', 'vegt', 'Veg 1', 'bag', '28'),
('vg-01-010', 'vegt', 'Veg 1', 'bag', '29'),
('vg-01-011', 'vegt', 'Veg 1', 'bag', '30'),
('vg-01-012', 'vegt', 'Veg 1', 'bag', '31'),
('vg-01-013', 'vegt', 'Veg 1', 'bag', '32'),
('vg-01-014', 'vegt', 'Veg 1', 'bag', '33'),
('vg-01-015', 'vegt', 'Veg 1', 'bag', '34'),
('vg-01-016', 'vegt', 'Veg 1', 'bag', '35'),
('vg-01-017', 'vegt', 'Veg 1', 'bag', '36'),
('vg-01-018', 'vegt', 'Veg 1', 'bag', '37'),
('vg-01-019', 'vegt', 'Veg 1', 'bag', '38'),
('vg-01-020', 'vegt', 'Veg 1', 'bag', '39');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `fk_customerid` FOREIGN KEY (`customerid`) REFERENCES `customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_sku` FOREIGN KEY (`sku`) REFERENCES `product_en` (`sku`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `deliveryaddress`
--
ALTER TABLE `deliveryaddress`
  ADD CONSTRAINT `fk_customer` FOREIGN KEY (`customerid`) REFERENCES `customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `orderdetail`
--
ALTER TABLE `orderdetail`
  ADD CONSTRAINT `fk_order` FOREIGN KEY (`orderid`) REFERENCES `order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_en`
--
ALTER TABLE `product_en`
  ADD CONSTRAINT `fk_category` FOREIGN KEY (`categorycode`) REFERENCES `category_en` (`code`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
